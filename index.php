<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Ferdian Novendra</title>
<meta name="description" content="Punya noven!" />
<!-- <link rel="shortcut icon" href="assets/images/logo-white-sm.png"> -->
<link rel="stylesheet" href="assets/css/master.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>

<div class="wrapper"> 
<?php 
$mysqli = new mysqli("localhost", "ferdiannovendra", "noven", "ferdian");

if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = "select * from biodata";
$result = $mysqli->query($query);
$row = $result->fetch_assoc();


?>
<nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full">
    <!--=== Start Top Search ===-->
  <div class="fullscreen-search-overlay" id="search-overlay"> <a href="#" class="fullscreen-close" id="fullscreen-close-button"><i class="mdi mdi-close"></i></a>
    <div id="fullscreen-search-wrapper">
      <form method="get" id="fullscreen-searchform">
        <input type="text" value="" placeholder="Type and hit Enter..." id="fullscreen-search-input" class="search-bar-top">
        <i class="fullscreen-search-icon mdi mdi-magnify">
        <input value="" type="submit">
        </i>
      </form>
    </div>
  </div>
  <!--=== End Top Search ===-->
  
  <div class="container"> 
    <!--=== Start Atribute Navigation ===-->
   
    <!--=== End Atribute Navigation ===--> 
    
    <!--=== Start Header Navigation ===-->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="mdi mdi-menu"></i> </button>
      <div class="logo logo-sm"> <a href="index.html"> <img class="logo logo-display" src="assets/images/logo-white-sm.png" alt=""> <img class="logo logo-scrolled" src="assets/images/logo-black-sm.png" alt=""> </a> </div>
    </div>
    <!--=== End Header Navigation ===--> 
    
    <!--=== Collect the nav links, forms, and other content for toggling ===-->
    <div class="collapse navbar-collapse" id="navbar-menu">
      <ul class="nav navbar-nav navbar-right" data-in="fadeIn" data-out="fadeOut">
        <!-- <li class="dropdown"> <a href="#" >Beranda</a> -->
        <!-- </li> -->
        <!-- <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Work</a>
          <ul class="dropdown-menu">
            <li><a href="work-default.html">Work Default</a></li>
            <li><a href="work-fullwidth.html">Work Fullwidth</a></li>
            <li><a href="work-metro.html">Work Metro</a></li>
          </ul>
        </li>
        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Our Blogs</a>
          <ul class="dropdown-menu">
            <li><a href="blog-default.html">Blog Default</a></li>
            <li><a href="blog-details.html">Blog Details</a></li>
          </ul>
        </li>
        <li><a href="our-services.html">Our Services</a></li>
        <li><a href="contact-us.html">Contact Us</a></li> -->
      </ul>
    </div>
    <!--=== /.navbar-collapse ===--> 
  </div>
    
</nav>
<!--=== Header End ===--> 

<!--=== Hero Style Two Start ===-->

<section class="hero-style-two pt-0 pb-0">
    
                <div class="rounded-triangle rd-imgs">
                    <div class="hero-text-wrap">
        <div class="hero-text text-left">
          <div class="container">
            <div class="white-color">
              <!-- <h3 class="upper-case">Warta Ubaya</h3> -->
              <h1 class="upper-case font-600">.<?php echo $row['nama'];?></h1>
              <h3 class="upper-case"><span class="font-600"><?php echo $row['nrp'];?></span> </h3>
              <p class="text-left mt-50">
                  <!-- <a class="btn btn-pink btn-md btn-animated-none">Tentang Kami</a>  -->
                <br>
                  <!-- <a class="btn btn-pink btn-md btn-animated-none">Pengajuan Liputan</a>  -->
                  <div class="call-us">
                      <h5><?php echo $row['alamat'];?></h5>
                      <h5><?php echo $row['kota'];?></h5>
                   
                      <h6>Apabila ada pertanyaan menariq untuk saia</h6>
                  </div>
              </p>
            </div>
          </div>
        </div>
      </div>
                </div>
            
</section>

<!--=== Hero Sytle Two End ===--> 
</div>
